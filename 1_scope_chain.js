'use strict'

var foo = "bar";

function baz(){
	// var foo = "baz"
	console.log("foo");
}

baz();


// Dependiendo de lo que pase en la linea 6, se arrojaran resultados distintos 
// Si se activa la linea 6, el resultado de la linea 10 sera imporimir la cadena "baz"
// caso contrario, se imprimira la cadena "bar" por la declaracion de la linea 3