'use strict';

function Persona(nombre, edad){
	// Variables privadas
	function greetings(nombre){
		return `my name is ${nombre}`
	}

	// Variables publicas
	this.nombre = nombre;
	this.edad = edad;
	this.presentacion = function(){
		return `hi ${greetings(this.nombre)}`
	}
}

const persona1 = new Persona('Ricardo', 22);
console.log(persona1.presentacion());

function getSalary(){
	return 'misery';
};

getSalary.Employ = function(){
	return 'misery';
};

getSalary.CEO = function(){
	return 'OFG';
};

console.log(getSalary.Employ(), getSalary.CEO());