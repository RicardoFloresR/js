'use strict';
const urls = [
  'https://jsonplaceholder.typicode.com/users',
  'https://jsonplaceholder.typicode.com/posts',
  'https://jsonplaceholder.typicode.com/albums'
]

const getData = async function() {
  const [ users, posts, albums ] = await Promise.all(urls.map(async function(url){
    // debugger;
      const resp = await fetch(url)
      const data = await resp.json();
      return data;
      // .then(resp => resp.json())
  }));
  console.log('users', users);
  console.log('posta', posts);
  console.log('albums', albums);
}
getData();
