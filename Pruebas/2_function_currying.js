'use strict';

function sum(a, b){
	return a + b;
};

const sumCopy = sum.bind(this, 2);

console.log(sumCopy(3));