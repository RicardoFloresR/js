'use strict';

const person = {
	name : 'Bruce',
	lastname : 'Wayne',
	fullname: function(){
		return this.name + ' ' + this.lastname
	}
};
const person2 = {
	name : 'Pablo',
	lastname : 'Wayne',
	fullname: function(){
		return this.name + ' ' + this.lastname;
	}
};

const person3 = {
	name: 'Ricardo',
	lastname : 'Flores'
}

const print = function(greet, adj){
	console.log(greet, this.fullname(), 'you are', adj);
};


//Como condicionar el contexto con la misma funcion ?? (Acceder a los datos de uno u otro objeto)

print.call(person, 'hello', 'special from call');

print.apply(person, ['hello', 'special from apply']);

// Como obtener una funcion de un objeto y que use las propiedades de un segundo? 

let result = person.fullname.apply(person3);
console.log(result);












