'use strict';

const createFunctions = function(){
	let array = [];
	let i;
	for(i = 0; i < 3; i++){
		console.log('la i dentro del for vale: ' + i);
		array.push(function(){
			console.log('la i dentro de la funcion ahora vale: ' + i);
	    });	
    };
	return array;
}

let closureVariable = createFunctions();

closureVariable[0]();
closureVariable[1]();
closureVariable[2]();

// Un closure es una funcion que retorna una funcion. Cómo es esto? 
// La funcion retornada utilizara variables del contexto de la funcion original 
// En este caso, las funciones anonimas que se estan introduciendo dentro del arreglo estan referenciando una variable que solo
//  existe en el contexto de la funcion padre createFunctions() y cuando esta es ejecutada en la linea 15, su contexto se crea y una vez 
//  termine de ser ejecutada, su contexto se eliminara junto con todas las variables locales que esta contiene, entonces, que pasa?
//  la variable que se esta referenciando se quedo guardada en memoria porque el lñenguaje sabe que quiza quiera utlizarla despues 
//  ya que asi lo declaraste en la funcion createFunctions


//Dato curioso: Las constantes no puede cambiar literalmente (const constante = 'algo'; constante = 23) pero,
// en el caso de que sean funciones, se les pueden agregar atributos cumpliendo con las propiedades de las first class functions 
const test = function(){
	console.log('test working!!!');
};

test.propiedad = 'otra propiedad';



