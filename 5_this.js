// 'use strict';

// nota: this es mucho mejor implementado cuando se usa 'let' en vez de 'var' debido a problemas con el entorno global 

function padre(){
	return this.nombre;
}

function hijo(){
	atributo = 'atriubuto del hijo';
	console.log(this.atributo);
}
// Si se ejecuta esta funcion, y el modo estricto esta activo, no encontrara la propiedad que se esta axigiendo (en este caso sera el atributo 'atributo')
// en caso contrario, encontrara la propiedad exigida. this, por default, sera el objeto global window a no ser que se encuentre en un contexto es especifico
// hijo();

let objeto = {
	nombre : 'ricardo flores ramirez',
	funcion : function(){
		return this.nombre;
	}
}

console.log(padre.call(objeto));

console.log(objeto.funcion());
// Si se usa el modo estricto, this hara referencia a cualquier valor, mientras que, en modo no estricto, hara referencia a cualquier objeto

let obj = {a: 'Custom'};

// This property is set on the global object
let a = 'Global';

function whatsThis() {
  return console.log(this.a);  // The value of this is dependent on how the function is called
}

whatsThis();          // 'Global'
whatsThis.call(obj);  // 'Custom'
whatsThis.apply(obj); 


