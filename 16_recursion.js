'use strict';

let i = 1;
function mult(...rest){
	// debugger;
	let x = rest.shift();
	i = i * x;
	if(rest.length != 0){
		mult(...rest);
	};
    return i;
}
let result = mult(2, 30, 1, 2, 3, 4);
console.log(result);

// const obj = {
// 	name: 'ricardo',
// 	lastname : 'flores',
// 	getName : function(){
// 		// debugger;
// 		console.log(this.name + ' ' + this.lastname);
// 	}
// }

// obj.getName();