'use strict';


const edades = [
{
	name : 'wendy',
	edad : 21
},
{
	name: 'alfredo',
	edad: 19
},
{
	name: 'Ricardo',
	edad: 21
}
];
const repuesto = [
{
	name : 'wendy',
	edad : 21
},
{
	name: 'Ricardo',
	edad: 21
}
]
let aux;
// array.filter() retorna un nuevo arreglo con todos los miembros que cumplan la condicion especificada
console.log(edades.filter(objeto => objeto.edad > 20));
// array.find() regresa el primer objeto que cumpla con las condiciones especificadas, en caso de no encontrarlo, retorna undefined
console.log(edades.find(objeto => objeto.edad > 20));
// 


const array1 = [1, 2, 3, 4];
const array2 = ['hola ', 'tio ', 'como ', 'estas?'];

// Array.reduce retorna un unico valor (el acumulador) el cual almacenara los datos que se le indiquen
aux = array2.reduce((acc, cv) => acc + cv);
console.log(aux);
const reducer = (accumulator, currentValue) => accumulator + currentValue;
console.log(array1.reduce((reducer)));

// Array.flat 'aplana' un arreglo dependiendo el nivel de profundidad que se especifique en su parametro, 
const nested = [[2, 3], 2, [1, [2, 3]]];
console.log(nested);
console.log(nested.flat(1));
console.log(nested.flat(2));

// array.findIndex retorna el index del primer elemento que coincida con las caracteristicas especificadas en  el callback
// , de otro moodo, retorna -1
aux = array1.findIndex((item) => item > 10); //Verifica si existe un numero mayor a 10 dentro del arreglo
console.log(aux);

// array.map retorna un nuevo array con los resultados que retorne el callback
aux = edades.map(item => item.edad); //Se crea un array nuevo con las edades de cada objeto del arreglo
console.log(aux);

// Array.join convierte todos lo elemtos de un arreglo en una cadena de texto. Si un elemento no esta definido o el nulo
// este se convierte en una cadena vacia
aux = nested.join('  -  ');
console.log(aux); //Funciona para arreglos con valores simples e incluso arrays anidados

// array.sort acomoda los elementos por caracter UNICODE, a no ser que se especifique de otra forma, este metodo ALTERA la estructura del
// arreglo original 
array1.sort(a => b => a - b);
console.log(array1);

// Array.some retorna un booleano dependiendo de si al menos un elemento cumple con la condicion especificada en el callback 
aux = edades.some(item => item.edad > 20); //Deberia retornar true
console.log(aux);

// Array.every es similar al array.some solo que en este caso todos los elementos deben cumplir con las condiciones dadas en  el callback
aux = edades.every(item => item.edad > 20); //Deberia retornar false
console.log(aux);

// Array.concat realiza una fusion superficial entre 2 arreglos
let complejo = [1, 2, {a : 'a'}];
aux = [].concat(complejo);
aux[2].a = 'z';
console.log(complejo, aux);

// Array.shift REMUEVE y retorna el primer elemento de un arreglo
aux = edades.shift();
console.log(aux, edades);

// Array.pop REMUEVE y retorna el ultimo elemento de un arrelgo
aux = edades.pop();
console.log(aux, edades);

// Array.forEach ejecuta una funcion una vez por cada elemento del arreglo 
repuesto.forEach(item => {
	edades.push(item);
});
console.log(edades);

// Array.includes retorna un booleano dependiendo si el elemento mandado como parametro se encuentra dentro del arreglo
aux = array1.includes(4); //Deberia retornar true
console.log(aux);







