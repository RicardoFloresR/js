'use strict';

var module = {
  x: 42,
  getX: function() {
    return this.x;
  }
}

const unboundGetX = (objeto) => {
	return objeto.getX();
}

console.log(unboundGetX(module));