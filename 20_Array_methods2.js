'use strict';
// -----------------
// Array.sort
// -----------------
const array = ['a', 'bb', 'c', 'dddd', 'ee', 'fff', 11111];
console.log('Antes de sort', array);
array.sort((a, b) => b.toString().length - a.toString().length);
console.log('Despues de sort', array);
// -----------------
// Array.concat
// -----------------
const numbers1 = [1, 2, 3];
const numbers2 = [4, 5, 6];
console.log('Array concat', numbers1.concat(numbers2),  numbers1, numbers2);
// -----------------
// Array.every
// -----------------
console.log('Array every', numbers1.every(item => item < 5), numbers1.every(item => item > 5));
// -----------------
// Array.some
// -----------------
console.log('Array some', numbers1.some(item => item > 5), numbers1.some(item => item < 2));
// -----------------
// Array.find
// -----------------
const finded = numbers1.find(item => item > 1);
console.log('Array find', finded, numbers1);
// -----------------
// Array.findIndex
// -----------------
const index = numbers1.findIndex(item => item > 1);
console.log('Array index', index, numbers1);
// -----------------
// Array.includes
// -----------------
console.log('Array includes', numbers1.includes(1));
// -----------------
// Array.reduce
// -----------------
const reducer = numbers1.reduce((acc, cv) =>{
	acc.push(cv);
	return acc;
}, {
	maximum : getMaximun,
	average : getAverage,
	minimum : getMinimum
});
console.log(reducer);