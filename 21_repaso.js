'use strict';
// ------------------------
// Diferencias entre let/const y var
// ------------------------
var globalObject = 'objeto global';
function foo(){
	var	 global = 'global en foo'; 
	console.log(global);
	if(true){
		var	 global = 'global en if'; //Es la misma variable
		console.log(global);
	};
	console.log(global);
}
foo();
console.log(window.globalObject);

// Temporal dead zone

try{
	console.log(a, b, c);

	var a = 'something';
	let b = 'something else';
	const c = 'const';
}catch(e){
	console.log('Temporal dead zone: ', e);
	// notese que el error se dispara al intentar accesar a "b" y no a "a"; "a" en ese momento es undefined 
	// esto se debe al proceso de HOISTING
}

function prueba(){
   var num = 33;
   if (true) {
      let num = (num + 55);//ReferenceError: no se puede acceder a la declaración léxica `num'antes de la inicialización 
      console.log(num);
   }
}
try{
	prueba();
}catch(e){
	console.log('Temporal dead zone: ', e);
}



