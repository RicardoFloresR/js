'use strict';

let a, b, excedente;

[a, b] = [20, 30];

console.log(a, b);

[a, b, ...excedente] = [10, 20, 30, 40, 50];
console.log(a, b); 
console.log(excedente);

let objeto = {
	a : 'letra a',
	b : 'letra b',
	c : 'letra c',
	d : 'letra d'
};

({a, b} = objeto);
console.log(a, b);

({a, b, ...excedente} = objeto);
console.log(a, b, excedente);