'use strict';

// La programacion funcional es un paradigma de programacion que se resume en saber algo pero no saber el porque
// Noralmente, al realizar un procedimiento tenemos una funcion enorme que realiza todos los pasos necesarios para cumplir la tarea
//  en el caso de la programacion funcional

// Pilares de la programacion funcional:
// Inmutabilidad: Evitar que los datos originales (la variable original) no sea alterada al momento de realizar operaciones con sus valores
//  esto suele pasar a menudo al momento de pasar un array u objeto como parametro de una funcion
let array = ["1", "2", "3"];
let arrayM = array.map((item) => parseInt(item));
console.log(array);
console.log(arrayM);

// Funciones puras: Una funcion pura es aquella funcion que su resultado depende enteramente de sus valores de entrada (determinista)
let sum = (a) => console.log(a*2);
sum(2);

// Funciones de orden superior: Son aquellas funciones que reciben funciones como argumento (callbacks) o que retornan 
//  una funcion (posiblemente closures)
console.log([1, 2, 3].filter(/*lo siguiente es el callback: */n => n>1));

// Currying: Asi se le define al acto de convertir una funcion de multiples variables en una secuencia de funciones unarias
const mult = (a) => (b) => a*b; //los argumentos de la funcion estan divididos por partes 
const multBy5 = mult(5);
console.log(multBy5(5));

// Composicion: La composicion es simplemente aplicar varias varias funciones en cadena comenzando con unoi o varios argumentos
// los cuales son manipulados y retornados por cada funcion (Parecido a las promesas al aplicar .then())
const original = [80, 3, 14, 22, 30];

const result = original
    .filter((value) => value%2 === 0)
    .filter((value) => value > 20)
    .reduce((accumulator, value) => accumulator + value);

console.log(result);
