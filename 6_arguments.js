'use strict'
var myfunction = function(a, b, c){
	console.log(arguments[0]);
	console.log(arguments[1]);
	console.log(arguments[2]);
}

myfunction('uno', 'dos', 'tres');
console.log('-----------------------------------------------');

// arguments es un arreglo que se genera automaticamente al ejecutar una funcion
// el arreglo tendra como contenido todos los argumentos que reciba la funcion en su respectivo orden
// NOTA: Este objeto-arreglo solo se genera en funciones 'clasicas'. No se genera dentro de arrow-functions

// Los argumentos de una funcion tambien pueden ser reasignados (podria servir para validar y reasignar un valor en caso de que no cumpla las condiciones o 
// realizar operaciones con el arreglo aunque estrictamente NO LO ES)

myfunction.cambiarArgumento = function(a, b, c){
	if(isNaN(arguments[0])){
		arguments[0] = 'nuevo argumento';
	}
	for(let item of arguments){
		console.log(item);
	}
	console.log('argumentos originales (las variables): ' + a +', ' + b + ', ' + c);
}

myfunction.cambiarArgumento('uno', 'dos', 'tres'); //Imprimira lops valores 'nuevo argumento', 'dos' y 'tres'
// Nota: en modo no estricto, si cambias el valos de un miembro de arguments, tambien cambiara la variable que se esta creando dentro de la funcion
// en esta caso es las variable 'a' la que cambia su valor. Si quieres evitar eso, debes usar argumentos rest o default
console.log('-----------------------------------------------');
// Si se quieren realizar operaciones propias de un arreglo, siempre puedes incluir todos los elementos de arguments en un array nuevo a traves de varios metodos existentes

// arguments es un objeto muy util en casos donde la funcion tiene un numero variable de argumentos


function list(type) {
  var html = '<' + type + 'l><li>';
  var args = Array.prototype.slice.call(arguments, 1);
  console.log(args);
  html += args.join('</li><li>');
  html += '</li></' + type + 'l>'; // end list

  return html;
}

var listHTML = list('u', 'One', 'Two', 'Three');
console.log(listHTML);

