'use strict';
const urls = ['https://jsonplaceholder.typicode.com/users', 
				'https://jsonplaceholder.typicode.com/albums', 
				'https://jsonplaceholder.typicode.com/posts'];
fetch('https://jsonplaceholder.typicode.com/users')
.then(jsonText => jsonText.json())
.then(data => {
	const  usersData = [...data];
	console.log(usersData);
	const usersDiv = document.getElementById('users');
	usersData.forEach(user => {
		const newDiv = document.createElement('div');
		newDiv.classList.add('user-div');
		const userName = document.createTextNode(user.name);
		newDiv.appendChild(userName);
		usersDiv.appendChild(newDiv);
	});
	document.getElementById('loading').style.display = 'none';
})
.catch(error => {console.log(error)})
// El bloque finally ejecuta un bloque de codigo sin importar si la promesa tuvo un error o fue exitosa
.finally(data => console.log('it worked!'))

async function getData(){
	await fetch('https://jsonplaceholder.typicode.com/posts')
	.then(jsonText => jsonText.json())
	.then(data => {
		console.log(data);
	});
	await fetch('https://jsonplaceholder.typicode.com/albums')
	.then(jsonText => jsonText.json())
	.then(data => {
		console.log(data);
	});
};
// getData();

const getData2 = async function(urls){
	const array = urls.map(item => fetch(item));
	for await(let request of array){
		const data = await request.json();
		console.log(data);
	}
}

getData2(urls);


// fetch('https://jsonplaceholder.typicode.com/todos/1')
// .then(response => response.json())
// .then(user => {
// 	console.log(user)
//     return fetch('https://jsonplaceholder.typicode.com/posts?userId=' + user.userId)
//            .then((response) => response.json())
//            .then(data => {
//            	console.log(data)
//            });

// });