'use strict'


var startTime = Date.now();
var boton = document.getElementById('boton');
let i = 0;
var imprimir = (e) => () => console.log('Funcion imprimir: ', e);
boton.addEventListener('click', imprimir('desde boton'));

console.log("inicio");
while((Date.now() - startTime) < 1000){
	imprimir('desde el while')();

}
imprimir('desde fin del while')();
