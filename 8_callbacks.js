'use strict'
function greeting(name) {
  console.log('Hello ' + name);
}

function processUserInput(callback) {
  var name = prompt('Please enter your name.');
  callback(name);
}

processUserInput(greeting);

// Una funcion de callback es simplemente pasar una funcion como argumento de otra, esto para poder utilizarla dentro de ella 
