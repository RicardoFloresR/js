'use strict';
// FORMA NATIVA DE CREAR UN COMPONENTE
class HolaComponentes extends HTMLElement {
  constructor() {
    super();
    this.attachShadow( {mode: 'open'} ).innerHTML = '<p>Hola! soy un Custom Element!</p>';
  }
}
customElements.define('hola-componentes', HolaComponentes);

