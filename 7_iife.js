'use strict'
var numero = 45;

var objeto = {
	nombre : 'ricardo',
	funcion : (function (a = 23) {
    var aName = "Barry";
    console.log(a);
     })()
};

// console.log(aName);
// Una iife (immediately invoked function expression) es una funcion de javascript que se ejecuta tan pronto como es definida
// Al igual que las funciones normales, no se pueden accesar a variables que esten defiunidas dentro de ellas pero si puedes accesar
// a variables que esten declaradas fuera de ellas (scope chain)
// Estas no pueden recibir algun argumento que no este definido ya que se ejecutan inmediatamente

// Si asignas una iife a una variable, esta tomara el valor de retorno que tenga la funcion y no la funcion como tal 

var variable = (function(){
	name = 'ricardo flores ramirez';
	return name;
})();
console.log(variable);