'use strict';

const objeto = {
	name : 'ricardo',
	lastname : 'flores'
};
const obj = {};
const nuevoObjeto = {...objeto};
// el spread operator evita que el objeto original sera modificado al momento de modificar el nuevop objeto
nuevoObjeto.lastname = 'ramirez';
console.log(nuevoObjeto, objeto);
