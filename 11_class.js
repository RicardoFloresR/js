// 'use strict';

// Hay 2 formas de declarar clases en JS: class expression y class declaration/statement

//class statement/declaration
class rectangle {
	constructor(height, width){
		this.height = height;
		this.width = width;
	};
	// Getter
  get area() {
    return this.calcArea();
  	};
  // Method
  calcArea() {
    return this.height * this.width;
  	};
};
console.log(rectangle.name, rectangle);

// class expression. De esta forma, el nombre de la variable que almacenara la clase tambien sera
//  el nombre de la clase y de puede acceder a ella a traves de la propiedad name (cuadrado.name)
const cuadrado = class{
	constructor(measure){
		this.height = measure;
		this.width = measure;
	};
	 // metodo estatico
	static sayHi(){
		console.log('Hi!');
	};
	static staticThis(){
		console.log(this);
	};
	sayThis(){
		console.log(this);
	};
};
console.log(cuadrado.name, cuadrado);
// A diferencia de las function statement, las class statement no son alocadas en la memoria en la estapa del hoisting
// El cuerpo de las clases sera ejecutado en modo estricto

// Los metodos estaticos permiten acceder a metodos de una clase. Ojo, se accese a los metodos de la clase mas no se accede al
//  metodo de un objeto instanciado de una clase
cuadrado.sayHi();

// Si se llama un metodo estatico o prototipo de una clase (mas no una instancia), el valor de this sera undefined
// cuadrado.sayThis();  // Salida esperada: Error 	
// cuadrado.staticThis();  // Salida esperada: Error 	


























